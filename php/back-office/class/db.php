<?php
class db {

  /**
   * Exemple de connexion BDD
   * @var string
   */

  const HOST = 'localhost';
  const DATABASE = 'linagora';
  const USERNAME = 'root';
  const PASSWORD = '***';

  private $connection;

  public function __construct() {

    $host = self::HOST;
    $database = self::DATABASE;
    $username = self::USERNAME;
    $password = self::PASSWORD;

    $this->connection = new PDO("mysql:host=$host;dbname=$database", $username, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    $this->connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
  }

  /**
   * Méthode prepare/execute
   * @param  prepare $sql
   * @param  execute $args
   * @return requête
   */

  public function queryList($sql, $args) {

    $req = $this->connection->prepare($sql);
    $req->execute($args);

    return $req;
  }
}
?>
