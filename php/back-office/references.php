<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" href="favicon.ico">

    <title>Références - Linagora</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/narrow-jumbotron.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>

<body>
<div class="container"> <!-- container -->

    <header class="header clearfix"> <!-- header -->
        <h1 class="text-center">Références - Linagora</h1>
    </header> <!-- /header -->

    <main role="main"> <!-- main -->

        <table class="table table-responsive"> <!-- table -->
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Client</th>
                <th scope="col">Context</th>
                <th scope="col">Status</th>
                <th scope="col">Objective</th>
                <th scope="col">Presentation</th>
                <th scope="col">Output</th>
                <th scope="col">CreatedAT</th>
            </tr>
            </thead>

            <tbody>
            <td>
                <!-- ID : Exemple -->
                1
            </td>
            <td>
                <!-- Client : Exemple -->
                Médecins du Monde
            </td>
            <td>
                <!-- Context : Exemple -->
                Médecins du Monde (ONG) - Association indépendante, Médecins du Monde agit au-delà du soin. Elle dénonce les atteintes à la dignité et aux droits de l'homme et se bat pour améliorer la situation des populations. Dans le cadre de sa transformation digitale globale, l'ONG Médecins du Monde désire réaliser la refonte de son site internet. Le projet global était piloté par DDB.
            </td>
            <td>
                <!-- Status : Exemple -->
            </td>
            <td>
                <!-- Objective : Exemple -->
                L'objectif est de véhiculer une image « actuelle » : système de navigation moderne accessible depuis un ensemble de terminaux très divers. Il s'agit de réaliser un accompagnement à la fois technique et méthodologique.
            </td>
            <td>
                <!-- Presentation : Exemple -->

                Le projet a consisté à : - Réaliser les spécifications techniques de la plate-forme ; - Assurer les développements techniques HTML du site ; - Assurer l'intégration du site.
            </td>
            <td>
                <!-- Output : Exemple -->
                Précisions sur le site web : - Site multi langues. - Responsive design : 2 points de rupture seront implémentés sur le site. - Compatible avec IE 9, 10, 11 ; FF, Chrome et Android 4. - Formulaire de contact multi-destinataire. - Inscription à des évènements avec compteur et email de remerciement (création/affichage d'évènement, formulaire d'inscription). - Module de cartographie avec affichage de puces sur les différents pays ou régions dans lesquels l'association intervient. - Mise en place de pages interfacées avec des Webservices de la solution « profilsoft » pour la création d'un section « offre d'emploi ».
            </td>
            <td>
                <!-- CreateAT : Exemple -->
            </td>
            <td>
                <!-- View -->
                <a href="?view=" class="btn btn-primary">View</a>
            </td>
            <td>
                <!-- Edit -->
                <a href="?edit=" class="btn btn-success">Edit</a>
            </td>
            <td>
                <!-- Delete -->
                <a href="?delete=" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this reference ?')">Delete</a>
            </td>
            </tbody>
        </table> <!-- /table -->

        <div class="row">
            <div class="col-lg-12">
                <form action="partials/add.php" method="POST">

                    <h4>Client</h4>
                    <input type="text" class='form-control' id=client name='client' placeholder="Client" required>

                    <h4>Status</h4>
                    <select class='form-control' name='status'>
                        <option value="publish">Publish</option>
                        <option value="unpublished">Unpublished</option>
                        <option value="draft">Draft</option>
                    </select>
            </div>

            <div class="col-lg-6">
                <h4>Context :</h4>
                <textarea type="text" class='form-control' id='context' name='context' required></textarea>

                <h4>Project presentation :</h4>
                <textarea type="text" class='form-control' id='presentation' name='presentation' required></textarea>
            </div>

            <div class="col-lg-6">
                <h4>Objective :</h4>
                <textarea type="text" class='form-control' id='objective' name='objective' required></textarea>

                <h4>Output</h4>
                <textarea type="text" class='form-control' id='outputref' name='outputref' required></textarea>
            </div>

            <div class="col-lg-12">
                <input type="submit" class="btn btn-success" value="Add reference">
            </div>
            </form>
        </div>
    </main>

    <footer class="footer"> <!-- footer -->
        <p>&copy; OpenHackademy</p>
    </footer> <!-- /footer -->

</div> <!-- /container -->
</body>
</html>